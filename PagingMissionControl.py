"""
File: missionControl.py
Author: Max Breitmeyer
Date: 3-5-22
Description:
    Ingest status telemetry data and create alert messages
"""
from dateutil.parser import parse
import uuid
import json

"""
A class that holds data of each Component which holds 
component ID and a set of properties.
"""

class ComponentData:
    uniqueID = ''
    component = ''
    timestamp = ''
    redHighLimit = 0
    yellowHighLimit = 0
    yellowLowLimit = 0
    redLowLimit = 0
    rawValue = 0
    

#    Constructor
    def __init__(self, _component, _timestamp, _redHighLimit, _yellowHighLimit, _yellowLowLimit, _redLowLimit, _rawValue):
        self.uniqueID = uuid.uuid1()
        self.component = _component
        self.timestamp = _timestamp
        self.redHighLimit = _redHighLimit
        self.yellowHighLimit = _yellowHighLimit
        self.yellowLowLimit = _yellowLowLimit
        self.redLowLimit = _redLowLimit
        self.rawValue = _rawValue
    
    
#    String representation
    def __str__(self):
        return "" + self.component + "," + \
                    str(self.timestamp) + " [" + str(self.redLowLimit) + " <= " + str(self.rawValue) + " =>" \
                        + str(self.redHighLimit) +' ]'
    
#    A method that checks if the component violates any condition based on the component
    def checkViolationCondition(self):
        if(self.component == 'BATT' and self.rawValue <= self.redLowLimit):
            return True
        if(self.component == 'TSTAT' and self.rawValue >= self.redHighLimit):
            return True
        return False
    
    
#    A method that gets the severity based on the component
    def getseverity(self):
        if(self.component == 'BATT' and self.rawValue <= self.redLowLimit):
            return 'RED LOW'
        if(self.component == 'TSTAT' and self.rawValue >= self.redHighLimit):
            return 'RED HIGH'
        return ''


# A class that holds data of each Satellites which holds 
# satellite ID and a set of components.
class SatelliteData:
    satelliteID = ''
    components = {}
    
#    Constructor
    def __init__(self, _satelliteID):
        self.satelliteID = _satelliteID
        self.components = {}
       
    
#A method that parses a split token of single line of log
    def parseComponent(self, tokens):
        component = tokens[7]
        if(not component in self.components):
            self.components[component] = []
        time = parse(tokens[0], fuzzy=True)
        comp = ComponentData(component, time, float(tokens[2]), float(tokens[3]), float(tokens[4]), float(tokens[5]), float(tokens[6]))
        self.components[component].append(comp)


#A method that checks if the Individual Component violates condition
    def CheckIndividualComponentCondition(self, componentID, component):
        result = ''
        refTimestamp = None
        count = 0
        for comp in component:
            refTimestamp = comp.timestamp
            if(comp.checkViolationCondition()):
                count = 1
                for comp2 in component:
                    if not comp.uniqueID == comp2.uniqueID:
                        toDateTime = comp2.timestamp
                        timeDiff = toDateTime - refTimestamp
                        minutes = int(abs(timeDiff.total_seconds() / 60))
                        if minutes > 5 or not comp2.checkViolationCondition():
                            continue   
                        count += 1
                if count >= 3:
                    jsonObject = {
                      "satelliteId": int(self.satelliteID),
                      "severity": comp.getseverity(),
                      "component": comp.component,
                      "timestamp": str(refTimestamp.strftime("%H:%M:%S %f")[:-3])
                    }
                    return  jsonObject
        return None
        
    
#A method that checks if the any of the Component violates condition
    def CheckComponentCondition(self):
        overallResult = []
        for componentID in self.components:
            result = self.CheckIndividualComponentCondition(componentID, self.components[componentID])
            if(not result is None):
                overallResult.append(result)
        return overallResult
                

# A class that holds data of each Monitoring log Data  
class MonitoringData:
    satellites = {}

    
#    Constructor    
    def __init__(self):
        self.satellites = {}

#    Parse a single line of log line.
    def parseSystemLog(self, log):
        tokens = log.split()
        if(len(tokens) == 2):
            tokens = tokens[1].split('|')
            satelliteID = tokens[1]
            if not satelliteID  in self.satellites:
                self.satellites[satelliteID] = SatelliteData(satelliteID)
            self.satellites[satelliteID].parseComponent(tokens)

#    Check for each satellite if they encountered for a violation.
    def getAlertMessages(self):
        overallResult = []
        for satellite in self.satellites:
            result = self.satellites[satellite].CheckComponentCondition()
            if(not result is None):
                for tResult in result:
                    overallResult.append(tResult)        
        return overallResult
            
            

#A utility class used load the Diagnostic log file.
#The class loads the file line by line and parse the logs into MonitoringData.
class SatelliteDiagReader:   
#A utility method used load the Diagnostic log file.
#The class loads the file line by line and parse the logs into MonitoringData.

    def ReadSystemData(self):
        fileName = input("Enter the name of text file data you will be using: ")
        data = MonitoringData()
        with open(fileName) as f:
            lines = f.readlines()
            for line in lines:
                data.parseSystemLog(line)
        return data
        

if __name__ == "__main__":
    reader = SatelliteDiagReader()
    data = reader.ReadSystemData()
    print(json.dumps(data.getAlertMessages(), indent=4))

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
            
